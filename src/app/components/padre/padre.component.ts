import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css'],
})
export class PadreComponent implements OnInit {
  libro = {
    autor: 'Vishen Lakhiani',
    titulo: 'El codigo de las mentes extraordinarias',
    anio: 2016,
    paginas: 276,
    descripcion:
      'Aprende a pensar como las mentes creativas mas brillantes de nuestra era, a cuestionar, desafiar y crear nuevas reglas para conceptos como el amor, la educacion, el trabajo, la felicidad y el sentido de la vida',
  };

  animal = {
    animalNombre: 'Pavo Real',
    nombreCientifico: 'Pavo cristatus',
    alimentacion:'Omnivoro',
    habitat:'Zonas boscosas humedas',
    descripcion:'El pavo real comun es una ave de gran tamaño, conocida principalmente por su colorida y deslumbrante cola,que presenta unos vistosos patrones.'
  };
  constructor() {}

  ngOnInit(): void {}
}
